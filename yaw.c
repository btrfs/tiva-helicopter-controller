/*
 * yaw.c
 *
 *  Created on: 19/05/2023
 *      Author: root
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "utils/ustdlib.h"
#include "yaw.h"
#include "pwm.h"

static bool g_is_calibrated = false;
static int8_t g_old_phase = 0;
static int16_t g_yaw = 0;

static const int8_t phases[4] = {0,3,1,2};

int8_t
readPhase(void)
{
    return phases[GPIOPinRead (YAW_PORT_BASE, YAW_A_PIN) + GPIOPinRead (YAW_PORT_BASE, YAW_B_PIN)];
}

void
IntYawHandler(void)
{
    int8_t current_phase = readPhase();
    if (g_old_phase == 3 && current_phase == 0) {
        g_yaw = (++g_yaw) % 448;
    } else if (g_old_phase == 0 && current_phase == 3) {
        g_yaw = (--g_yaw) % 448;
    } else if (current_phase > g_old_phase) {
        g_yaw = (++g_yaw) % 448;
    } else if (current_phase < g_old_phase) {
        g_yaw = (--g_yaw) % 448;
    }
    if (g_yaw > 224) {
        g_yaw = g_yaw - 448;
    } else if (g_yaw < -224) {
        g_yaw = g_yaw + 448;
    }
    g_old_phase = current_phase;
    GPIOIntClear(YAW_PORT_BASE, YAW_A_PIN_INT | YAW_B_PIN_INT);
}

void
IntYawRef(void)
{
    g_is_calibrated = true;
    GPIOIntClear(YAW_REF_PORT_BASE, YAW_REF_PIN_INT);
}

int32_t
calibrateYaw(void)
{
    while (!g_is_calibrated) {
        setPWM_TAIL(45);
    }
    return getYaw();
}

void
initYaw(void)
{
    SysCtlPeripheralEnable (YAW_PERIPH);
    GPIOPinTypeGPIOInput (YAW_PORT_BASE, YAW_A_PIN | YAW_B_PIN);

    GPIOIntRegister(YAW_PORT_BASE, IntYawHandler);
    GPIOIntTypeSet (YAW_PORT_BASE, YAW_A_PIN | YAW_B_PIN, GPIO_BOTH_EDGES);
    GPIOIntEnable(YAW_PORT_BASE, YAW_A_PIN | YAW_B_PIN);

    GPIOPadConfigSet (YAW_PORT_BASE, YAW_A_PIN | YAW_B_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);


    SysCtlPeripheralEnable (YAW_REF_PERIPH);
    GPIOPinTypeGPIOInput (YAW_REF_PORT_BASE, YAW_REF_PIN);

    GPIOIntRegister(YAW_REF_PORT_BASE, IntYawRef);
    GPIOIntTypeSet (YAW_REF_PORT_BASE, YAW_REF_PIN, GPIO_BOTH_EDGES);
    GPIOIntEnable(YAW_REF_PORT_BASE, YAW_REF_PIN);

    GPIOPadConfigSet (YAW_REF_PORT_BASE, YAW_REF_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
}

int32_t
getYaw(void)
{
    return g_yaw * 100 * 360 / 448;
}

char*
getYawString(void)
{
    static char yaw_buffer[7];
    char negative = ' ';
    volatile int32_t angle = g_yaw * 100 * 360 / 448;
    int32_t angle_whole = abs(angle / 100);
    int32_t angle_frac = abs(angle % 100);
    if (angle < 0){
        negative = '-';
    }
    usnprintf(yaw_buffer, sizeof(yaw_buffer), "%c%d.%d", negative, angle_whole, angle_frac);
    return yaw_buffer;
}

void
resetYawRef(void) {
    g_is_calibrated = false;
}
