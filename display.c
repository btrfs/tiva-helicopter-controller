// *******************************************************
// 
// display.c
//
// Support for the OLED Display along with generalized
// function to edit the whole screen.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/21/2023
// *******************************************************

#include <stdint.h>
#include <stdbool.h>

#include "OrbitOLED/OrbitOLEDInterface.h"

#include "utils/ustdlib.h"
#include "display.h"

// *******************************************************
// initDisplay: Initialises the OLED display on orbit board.
//
//! \return None.
void
initDisplay(void)
{
  OLEDInitialise();
}

// *******************************************************
// clearScreen: Removes all text from the dispaly leaving it 
// empty.
//
//! \return None.
void
clearScreen(void)
{
    /* Clear string from line on OLED */
    OLEDStringDraw("                ", 0, 0);
    OLEDStringDraw("                ", 0, 1);
    OLEDStringDraw("                ", 0, 2);
    OLEDStringDraw("                ", 0, 3);
}

// *******************************************************
// displayScreen: Updates the screen to display a new altitude, yaw
// and the main rotors current duty cycle, respectively.
//
//! \param altitude is the current altitude percentage given from getAltitudePerc()
//! \param yaw is the current yaw angle displayed as a percentage from -180 - 180 degrees
//! \param main_duty_cycle is the current duty cycle of the main rotor and ranges from 2% - 98%
//
//! \return None.
void
displayScreen(int32_t altitude, char* yaw, uint8_t main_duty_cycle, uint8_t tail_duty_cycle)
{
  /* Display the corresponding attribute onto the OLED specified by g_displayScreen */

    char text_buffer[17];

    usnprintf(text_buffer, sizeof(text_buffer), "Altitude: %5d%%", altitude);
    OLEDStringDraw(text_buffer, 0, 0);

    usnprintf(text_buffer, sizeof(text_buffer), "Yaw: %7s deg", yaw);
    OLEDStringDraw(text_buffer, 0, 1);

    usnprintf(text_buffer, sizeof(text_buffer), "Main DC: %3d%%", main_duty_cycle);
    OLEDStringDraw(text_buffer, 0, 2);

    usnprintf(text_buffer, sizeof(text_buffer), "Tail DC: %3d%%", tail_duty_cycle);
    OLEDStringDraw(text_buffer, 0, 3);
}
