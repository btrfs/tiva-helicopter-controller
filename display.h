#ifndef DISPLAY_H_
#define DISPLAY_H_

// *******************************************************
// 
// display.h
//
// Support for the OLED Display along with generalized
// function to edit the whole screen.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/21/2023
// *******************************************************

//*****************************************************************************
// Constants
//*****************************************************************************
typedef enum
{
    ALTITUDE = 0,
    MEAN,
    OFF
} displayState_e;

// *******************************************************
// initDisplay: Initialises the OLED display on orbit board.
//
//! \return None.
void
initDisplay(void);

// *******************************************************
// clearScreen: Removes all text from the dispaly leaving it 
// empty.
//
//! \return None.
void
clearScreen(void);

// *******************************************************
// displayScreen: Updates the screen to display a new altitude, yaw
// and the main rotors current duty cycle, respectively.
//
//! \param altitude is the current altitude percentage given from getAltitudePerc()
//! \param yaw is the current yaw angle displayed as a percentage from -180 - 180 degrees
//! \param main_duty_cycle is the current duty cycle of the main rotor and ranges from 2% - 98%
//
//! \return None.
void
displayScreen(int32_t, char*, uint8_t, uint8_t);

#endif /* DISPLAY_H_ */
