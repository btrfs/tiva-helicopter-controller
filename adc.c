// *******************************************************
// 
// adc.c
//
// Support for the ADC along with functions to calculate
// altitude based on the current mean ADC and the reference
// ground voltage.
// 
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/21/2023
// *******************************************************

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "circBufT.h"
#include "adc.h"


static circBuf_t g_buffer;
static int32_t g_zeroRef;
static uint32_t sum;
static uint16_t i;

// *******************************************************
// ADCIntHandler: Runs everytime the ADC interrupt triggers,
// writes to CirBuf the current ADCSequenceData.
//
//! \note ADCIntClear must be called to clear the sample sequence
//  interrupt source, failure to do so will lead to the interrupt
//  being called immediately upon exit. 
//
//! \return None.
void
ADCIntHandler(void)
{
    uint32_t ulValue;

    ADCSequenceDataGet(ADC0_BASE, 3, &ulValue);
    writeCircBuf(&g_buffer, ulValue);
    //! \note Maybe move to top
    ADCIntClear(ADC0_BASE, 3);

}


// *******************************************************
// initADC: Initialise the ADC such that is samples at sequence 3.
// also initialise the circbuffer which is to be used when getting
// mean ADC.
//
//! \return None.
void
initADC (void)
{
  initCircBuf (&g_buffer, BUFFER_SIZE);
  /* Initialize the ADC such that it samples at sequence 3 */
  SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
  while (!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)) {}
  /* Configure ADC on sequence 3 to trigger on function call with highest (configurable) priority */
  ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
  /* Configure step 0 on sequence 3. Set to sample channel 9 (corresponds to AIN9/PE4) to single-ended mode and triggers interrupt flag when done. Also tells ADC this is the last conversion on sequence 3 */
  ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH9 | ADC_CTL_IE | ADC_CTL_END);

  ADCSequenceEnable(ADC0_BASE, 3);
  ADCIntRegister(ADC0_BASE, 3, ADCIntHandler);
  ADCIntEnable(ADC0_BASE, 3);
}

// *******************************************************
// calcMeanADC: Averages the value in circBuf over a range
//! of \b BUFFER_SIZE 
//
//! \note BUFFER_SIZE is added to the sum to ensure that the
//  final number is rounded.
//
//! \return returns the current Mean ADC as an int. This is
//  range of 0-4096.
int
calcMeanADC(void)
{
    sum = 0;
    for (i = 0; i < BUFFER_SIZE; i++)
        sum = sum + readCircBuf (&g_buffer);
    return (2 * sum + BUFFER_SIZE) / 2 / BUFFER_SIZE;
}

// *******************************************************
// resetZeroBuf: Updates the global zero reference, changing the
//! \b g_zeroRef to the current Mean ADC value, this leads the
//  current altitude output to reset to 0% height
//
//! \return None.
void
resetZeroRef(void) 
{
    g_zeroRef = calcMeanADC();
}

// *******************************************************
// getAltitudePerc: converts the current mean ADC value to
// a displayable percentage.
//
//! \note negative is used as a drop in voltage means an increase
//  in height. ADC value range is the total range of the ADC 0 - 4096
//  divided by the total range of voltage 0 - 3.3
//
//! \return Percentage of altitude as an int32_t.
int32_t
getAltitudePerc(void) 
{
    return -(calcMeanADC() * 100 / ADC_VALUE_RANGE - g_zeroRef * 100 / ADC_VALUE_RANGE);
}

// *******************************************************
// getAltitudePerc: converts the provided altitude percentage
// into an ADC value.
//
//! \param perc is an altitude percentage.
//
//! \return ADC value of given percentage as an int32_t.
int32_t
convertPercToADC(int32_t perc) 
{
    return perc * ADC_VALUE_RANGE / 100;
}
