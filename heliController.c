/******************************************************************************

 heliController.c - Program for Tiva processor to control the helicopter

 Authors:
     A. Sevilla (36541695)
     D. McNulty (57781020)
 Last modified:	05/23/2023

******************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/adc.h"
#include "adc.h"
#include "buttons.h"
#include "clock.h"
#include "display.h"
#include "piControl.h"
#include "switch.h"
#include "pwm.h"
#include "uart.h"
#include "yaw.h"

#define REFRESH_HZ 6
#define DISPLAYDELAY 500

void
initPeripherals(void)
{
    initClock();

    SysCtlPeripheralReset (PWM_MAIN_PERIPH_GPIO); // Used for PWM output
    SysCtlPeripheralReset (PWM_MAIN_PERIPH_PWM);  // Main Rotor PWM
    SysCtlPeripheralReset (PWM_TAIL_PERIPH_GPIO); // Used for PWM output
    SysCtlPeripheralReset (PWM_TAIL_PERIPH_PWM);  // Main Rotor PWM

    initADC();
    initButtons();
    initDisplay();
    initSwitch();
    initPWM();
    initUART();
    initYaw();

    IntMasterEnable();
}

void
startUp(void)
{
    delayHZ(REFRESH_HZ);
    resetZeroRef();
    resetYawRef();
}

int
main(void)
  {
    initPeripherals();
    startUp();
    int8_t altitude_perc = 0;
    char* yaw;
    int16_t yaw_angle = 0;
    int8_t main_duty_cycle = 0;
    int8_t tail_duty_cycle = 0;

    int8_t desired_altitude = 0;
    int16_t desired_yaw = 0;
    bool is_flying = false;

    uint16_t counter = 0;

    while (1)
    {
        // Check inputs
        updateButtons();
        if (checkButton(UP) == PUSHED && desired_altitude < 100)
            desired_altitude += 10;
        if (checkButton(DOWN) == PUSHED && desired_altitude > 0)
            desired_altitude -= 10;
        if (checkButton(LEFT) == PUSHED)
            desired_yaw = (desired_yaw + 15 * 100);
        if (checkButton(RIGHT) == PUSHED)
            desired_yaw = (desired_yaw - 15 * 100);
        if (checkButton(RESET) == PUSHED)
            SysCtlReset();
        updateSwitch();
        int8_t switch_state = checkSwitch();
        if (switch_state == 2) {
            desired_altitude = 0;
            is_flying = true;
        }
        else if (switch_state == 1)
            is_flying = false;

        // Calculate yaw
        if (desired_yaw > 180 * 100)
            desired_yaw -= 360 * 100;
        else if (desired_yaw < -180 * 100)
            desired_yaw += 360 * 100;
        yaw_angle = getYaw();
        yaw = getYawString();

        // Calculate altitude as a percentage
        altitude_perc = getAltitudePerc();

        // Output duty cycles to PWM generators
        if (is_flying) {
            // Calculate duty cycles
            main_duty_cycle = calcAltitudeDutyCycle(
                    convertPercToADC(desired_altitude), convertPercToADC(getAltitudePerc()));
            tail_duty_cycle = calcYAWDutyCycle(desired_yaw, yaw_angle);
            setPWM_MAIN(main_duty_cycle);
            setPWM_TAIL(tail_duty_cycle);
        } else {

            desired_yaw = calibrateYaw();
            if (altitude_perc >= 5)
                setPWM_MAIN(30);
            else {
//                finished_landing = true;
                setPWM_MAIN(0);
            }
            setPWM_TAIL(0);
        }

        // Send data to OLED and UART
        if (counter > DISPLAYDELAY) {
            displayScreen(altitude_perc, yaw, main_duty_cycle, tail_duty_cycle);
            UARTSendData(is_flying, altitude_perc, yaw, main_duty_cycle, tail_duty_cycle);
            counter = 0;
        }
        counter++;
    }
}
