// *******************************************************
// 
// piControl.c
//
// Support for the calculation of main and tail motor duty cycle
// using the PI control tuning.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/22/2023
//
// To Update:
// What are the unit for desired and current yaw + fix functions
//
// *******************************************************
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "piControl.h"
#include "uart.h"
#include "stdio.h"
#include "stdlib.h"

static int16_t main_integral_control = 0;
static int16_t tail_integral_control = 0;

// *******************************************************
// calcAltitudeDutyCycle: Calculates the duty cycle required to be outputed
// by the PWM so that the helicopter will reach the desired altitude based 
// on the current altitude.
//
//! \param desired_altitude is the altitude desired through the up and down buttons
//  to reach as a range of 0 - 1241
//
//! \param current_altitude is the current altitude as a range of 0 - 1241.
//
//! \return Duty cycle that is to bet set as an int8_t.
int8_t
calcAltitudeDutyCycle(int16_t desired_altitude, int16_t current_altitude)
{
    int16_t error = desired_altitude - current_altitude;
    int16_t proportional_control =  error * 100 / MAIN_CONTROL_GAIN_PROP;
    int16_t d_integral_control = error * 100 / MAIN_CONTROL_GAIN_INT;
    int16_t control = (proportional_control + main_integral_control + d_integral_control);

    if (control > MAIN_OUTPUT_MAX)
        control = MAIN_OUTPUT_MAX;
    else if (control < MAIN_OUTPUT_MIN)
        control = MAIN_OUTPUT_MIN;
    else
        main_integral_control += d_integral_control;

    return control;
}

// *******************************************************
// calcYawDutyCycle: Calculates the duty cycle required to be outputed
// by the PWM so that the helicopter will reach the desired yaw based 
// on the current yaw.
//
//! \param desired_yaw is the yaw desired through the left and right buttons
//  to reach as a range of ??????????????
//
//! \param current_yaw is the current yaw as a range of ????????????
//
//! \return Duty cycle that is to bet set as an int8_t.
int8_t
calcYAWDutyCycle(int16_t desired_yaw, int16_t current_yaw)
{
    int16_t error = desired_yaw - current_yaw;
    int16_t proportional_control =  error / TAIL_CONTROL_GAIN_PROP;
    int16_t d_integral_control = error / TAIL_CONTROL_GAIN_INT;
    int16_t control = (proportional_control + tail_integral_control + d_integral_control);

    if (control > TAIL_OUTPUT_MAX)
        control = TAIL_OUTPUT_MAX;
    else if (control < TAIL_OUTPUT_MIN)
        control = TAIL_OUTPUT_MIN;
    else
        tail_integral_control += d_integral_control;

    return control;
}
