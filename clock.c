// *******************************************************
// 
// clock.c
//
// Support for Main Oscillator (MOSC) using PLL
// output as the system clock with an external
// crystal of 16Mhz.
// 
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/21/2023
// *******************************************************

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/adc.h"
#include "clock.h"


// *******************************************************
// SysTickIntHandler: Trigger the ADC process each time the 
// systick interrupt triggers.
//
//! \return None.
void
SysTickIntHandler(void)
{
  ADCProcessorTrigger(ADC0_BASE, 3);
}

// *******************************************************
// initClock: Initialise the external crystal clock to 16Mhz,
// periodically call the ADC Process at the specified sample rate
//
//! \return None.
void
initClock(void)
{
  SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

  SysTickPeriodSet(SysCtlClockGet() / SAMPLE_RATE_HZ);
  SysTickIntRegister(SysTickIntHandler);
  SysTickIntEnable();
  SysTickEnable();
}

// *******************************************************
// delayHz: Provides a small delay
//
//! \param time will lead to a delay of 1/time seconds. Period calling
//  will lead to a delay of time Hz.
//
//
//! \note \b INSTRUCTION_PER_CYCLE is hardcoded amount of instructions
//  SysCtlDelay() uses to create a delay.
//
//! \return None.
void
delayHZ(uint32_t time) 
{
  SysCtlDelay(SysCtlClockGet() / (time * INSTRUCTIONS_PER_CYCLE));
}
