#ifndef SWITCH_H_
#define SWITCH_H_

/*
 * switch.h
 *
 *  Created on: 20/05/2023
 *      Author: root
 */

#define SWITCH_PERIPH  SYSCTL_PERIPH_GPIOA
#define SWITCH_PORT_BASE  GPIO_PORTA_BASE
#define SWITCH_PIN  GPIO_PIN_7
#define SWITCH_NORMAL  false

void
initSwitch(void);

void
updateSwitch(void);

int8_t
checkSwitch(void);

#endif /* SWITCH_H_ */
