/*
 * yaw.h
 *
 *  Created on: 19/05/2023
 *      Author: root
 */

#ifndef YAW_H_
#define YAW_H_

#include <stdbool.h>

// YAW
#define YAW_PERIPH  SYSCTL_PERIPH_GPIOB
#define YAW_PORT_BASE  GPIO_PORTB_BASE

#define YAW_A_PIN  GPIO_PIN_0
#define YAW_A_PIN_INT GPIO_INT_PIN_0

#define YAW_B_PIN  GPIO_PIN_1
#define YAW_B_PIN_INT GPIO_INT_PIN_1

#define YAW_REF_PERIPH  SYSCTL_PERIPH_GPIOC
#define YAW_REF_PORT_BASE  GPIO_PORTC_BASE

#define YAW_REF_PIN  GPIO_PIN_4
#define YAW_REF_PIN_INT GPIO_INT_PIN_4

int8_t
readPhase(void);

void
IntYawHandler(void);

void
IntYawRef(void);

void
initYaw(void);

void
resetYawRef(void);

char*
getYawString(void);

int32_t
getYaw(void);

int32_t
calibrateYaw(void);

void
setCalibration(void);

#endif /* YAW_H_ */
