/*
 * switch.c
 *
 *  Created on: 20/05/2023
 *      Author: root
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "switch.h"

static bool switch_state;
static bool switch_flag;

void
initSwitch(void)
{
    // UP button (active HIGH)
    SysCtlPeripheralEnable (SWITCH_PERIPH);
    GPIOPinTypeGPIOInput (SWITCH_PORT_BASE, SWITCH_PIN);
    GPIOPadConfigSet (SWITCH_PORT_BASE, SWITCH_PIN, GPIO_STRENGTH_2MA,
       GPIO_PIN_TYPE_STD_WPD);
    switch_state = SWITCH_NORMAL;
    switch_flag = false;
}

void
updateSwitch(void)
{
    bool switch_value;
    switch_value = GPIOPinRead (SWITCH_PORT_BASE, SWITCH_PIN) == SWITCH_PIN;
    if (switch_value != switch_state) {
        switch_flag = true;
        switch_state = switch_value;
    }
}

int8_t checkSwitch(void)
{
    if (switch_flag) {
        switch_flag = false;
        if (switch_state == SWITCH_NORMAL) {
            return 2;
        } else {
            return 1;
        }
    }
    return 0;
}
