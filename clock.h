#ifndef CLOCK_H_
#define CLOCK_H_

// *******************************************************
// 
// clock.h
//
// Support for Main Oscillator (MOSC) using PLL
// output as the system clock with an external
// crystal of 16Mhz.
// 
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/21/2023
// *******************************************************

//*****************************************************************************
// Constants
//*****************************************************************************
#define PWM_DIVIDER_CODE   SYSCTL_PWMDIV_4
#define SAMPLE_RATE_HZ 60
#define INSTRUCTIONS_PER_CYCLE 3

// *******************************************************
// SysTickIntHandler: Trigger the ADC process each time the 
// systick interrupt triggers.
//
//! \return None.
void
SysTickIntHandler(void);

// *******************************************************
// initClock: Initialise the external crystal clock to 16Mhz,
// periodically call the ADC Process at the specified sample rate.
//
//! \return None.
void
initClock(void);

// *******************************************************
// delayHz: Provides a small delay
//
//! \param time will lead to a delay of 1/time seconds. Period calling
//  will lead to a delay of time Hz.
//
//! \return None.
void
delayHZ(uint32_t time);

#endif /* CLOCK_H_ */
