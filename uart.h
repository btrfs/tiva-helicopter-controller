// *******************************************************
// 
// uart.h
//
// Support for sending data through UART to be read in computer
// terminal.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/22/2023
// *******************************************************
#ifndef UART_H_
#define UART_H_

//********************************************************
// Constants
//********************************************************
#define SYSTICK_RATE_HZ 100
#define SLOWTICK_RATE_HZ 4
#define MAX_STR_LEN 16
//---USB Serial comms: UART0, Rx:PA0 , Tx:PA1
#define BAUD_RATE 9600
#define UART_USB_BASE           UART0_BASE
#define UART_USB_PERIPH_UART    SYSCTL_PERIPH_UART0
#define UART_USB_PERIPH_GPIO    SYSCTL_PERIPH_GPIOA
#define UART_USB_GPIO_BASE      GPIO_PORTA_BASE
#define UART_USB_GPIO_PIN_RX    GPIO_PIN_0
#define UART_USB_GPIO_PIN_TX    GPIO_PIN_1
#define UART_USB_GPIO_PINS      UART_USB_GPIO_PIN_RX | UART_USB_GPIO_PIN_TX

// *******************************************************
// initUART: Initialises the UART to 8 bits, 1 stop bit
// and no parity. 
//
//! \return None.
void initUART (void);

// *******************************************************
// UARTSendData: Larger version of UARTSend for specific use
// in ENCE361 Helicopter project, sending the current altitude, yaw
// and main duty cycle through UART.
//
//! \param altitude is the current altitude as a range from 0 - 1241.
//! \param yaw is a pointer to the array holding the yaw as a percentage.
//! \param main_duty_cycle is the current duty cycle for the main rotor as a percentage.
//
//! \return None.
void UARTSendData (bool, int32_t, char*, uint8_t, uint8_t);
//void UARTSend (char*);

void
UARTSend (char *pucBuffer);

#endif /* UART_H_ */
