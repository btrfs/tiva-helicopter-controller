// *******************************************************
// 
// pwm.c
//
// Support for the pwm output of the main and tail rotor.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/22/2023
// *******************************************************

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "pwm.h"

// *******************************************************
// setPWM_MAIN: updates the PWM output of the main rotor, changing
// the duty cycle within a range of 2% - 98%.
//
//! \param duty_cycle is a number between 2% - 98% which is the total
//  time that the PWM generator is signal HIGH. 
//
//! \return None.
void
setPWM_MAIN (uint8_t duty_cycle)
{
//     Calculate the PWM period corresponding to the freq.
    uint32_t ui32Period =
        SysCtlClockGet() / PWM_DIVIDER / PWM_FREQ;

    PWMGenPeriodSet(PWM_MAIN_BASE, PWM_MAIN_GEN, ui32Period);
    PWMPulseWidthSet(PWM_MAIN_BASE, PWM_MAIN_OUTNUM,
                     ui32Period * duty_cycle / 100);
}

// *******************************************************
// setPWM_TAIL: updates the PWM output of the tail rotor, changing
// the duty cycle within a range of 2% - 98%.
//
//! \param duty_cycle is a number between 2% - 98% which is the total
//  time that the PWM generator is signal HIGH. 
//
//! \return None.
void
setPWM_TAIL (uint8_t duty_cycle)
{
    // Calculate the PWM period corresponding to the freq.
    uint32_t ui32Period =
        SysCtlClockGet() / PWM_DIVIDER / PWM_FREQ;

    PWMGenPeriodSet(PWM_TAIL_BASE, PWM_TAIL_GEN, ui32Period);
    PWMPulseWidthSet(PWM_TAIL_BASE, PWM_TAIL_OUTNUM,
                     ui32Period * duty_cycle / 100);
}

// *******************************************************
// initPWM: Initialises the PWM outputs of the main and tail rotor
//
//! \note Main Rotor Data as follows:
//  PWM Hardware Details M0PWM7 (gen 3)
//  ---Main Rotor PWM: PC5, J4-05
//
//! \note Tail Rotor Data as follows:
//  PWM Hardware Details M1PWM5 (gen 2)
//  ---Tail Rotor PWM: PF1, J3-10
//
//! \return None.
void
initPWM (void)
{
    SysCtlPWMClockSet(PWM_DIVIDER_CODE);

    // Main Motor
    SysCtlPeripheralEnable(PWM_MAIN_PERIPH_PWM);
    SysCtlPeripheralEnable(PWM_MAIN_PERIPH_GPIO);

    GPIOPinConfigure(PWM_MAIN_GPIO_CONFIG);
    GPIOPinTypePWM(PWM_MAIN_GPIO_BASE, PWM_MAIN_GPIO_PIN);

    PWMGenConfigure(PWM_MAIN_BASE, PWM_MAIN_GEN,
                    PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);

    setPWM_MAIN(0);

    PWMGenEnable(PWM_MAIN_BASE, PWM_MAIN_GEN);
    PWMOutputState(PWM_MAIN_BASE, PWM_MAIN_OUTBIT, true);


    // Tail Motor
    SysCtlPeripheralEnable(PWM_TAIL_PERIPH_PWM);
    SysCtlPeripheralEnable(PWM_TAIL_PERIPH_GPIO);

    GPIOPinConfigure(PWM_TAIL_GPIO_CONFIG);
    GPIOPinTypePWM(PWM_TAIL_GPIO_BASE, PWM_TAIL_GPIO_PIN);

    PWMGenConfigure(PWM_TAIL_BASE, PWM_TAIL_GEN,
                    PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);

    setPWM_TAIL(0);
    PWMGenEnable(PWM_TAIL_BASE, PWM_TAIL_GEN);
    PWMOutputState(PWM_TAIL_BASE, PWM_TAIL_OUTBIT, true);
}
