#ifndef PWM_H_
#define PWM_H_

// *******************************************************
// 
// pwm.h
//
// Support for the pwm output of the main and tail rotor.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/22/2023
// *******************************************************

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

// PWM configuration
#define PWM_DIVIDER_CODE   SYSCTL_PWMDIV_4
#define PWM_DIVIDER        4

//  PWM Hardware Details M0PWM7 (gen 3)
//  ---Main Rotor PWM: PC5, J4-05
#define PWM_MAIN_BASE        PWM0_BASE
#define PWM_MAIN_GEN         PWM_GEN_3
#define PWM_MAIN_OUTNUM      PWM_OUT_7
#define PWM_MAIN_OUTBIT      PWM_OUT_7_BIT
#define PWM_MAIN_PERIPH_PWM  SYSCTL_PERIPH_PWM0
#define PWM_MAIN_PERIPH_GPIO SYSCTL_PERIPH_GPIOC
#define PWM_MAIN_GPIO_BASE   GPIO_PORTC_BASE
#define PWM_MAIN_GPIO_CONFIG GPIO_PC5_M0PWM7
#define PWM_MAIN_GPIO_PIN    GPIO_PIN_5

//  PWM Hardware Details M1PWM5 (gen 2)
//  ---Tail Rotor PWM: PF1, J3-10
#define PWM_TAIL_BASE        PWM1_BASE
#define PWM_TAIL_GEN         PWM_GEN_2
#define PWM_TAIL_OUTNUM      PWM_OUT_5
#define PWM_TAIL_OUTBIT      PWM_OUT_5_BIT
#define PWM_TAIL_PERIPH_PWM  SYSCTL_PERIPH_PWM1
#define PWM_TAIL_PERIPH_GPIO SYSCTL_PERIPH_GPIOF
#define PWM_TAIL_GPIO_BASE   GPIO_PORTF_BASE
#define PWM_TAIL_GPIO_CONFIG GPIO_PF1_M1PWM5
#define PWM_TAIL_GPIO_PIN    GPIO_PIN_1

#define PWM_FREQ             250

// *******************************************************
// setPWM_MAIN: updates the PWM output of the main rotor, changing
// the duty cycle within a range of 2% - 98%.
//
//! \param duty_cycle is a number between 2% - 98% which is the total
//  time that the PWM generator is signal HIGH. 
//
//! \return None.
void
setPWM_MAIN (uint8_t);

// *******************************************************
// setPWM_TAIL: updates the PWM output of the tail rotor, changing
// the duty cycle within a range of 2% - 98%.
//
//! \param duty_cycle is a number between 2% - 98% which is the total
//  time that the PWM generator is signal HIGH. 
//
//! \return None.
void
setPWM_TAIL (uint8_t);

// *******************************************************
// initPWM: Initialises the PWM outputs of the main and tail rotor
//
//! \note Main Rotor Data as follows:
//  PWM Hardware Details M0PWM7 (gen 3)
//  ---Main Rotor PWM: PC5, J4-05
//
//! \note Tail Rotor Data as follows:
//  PWM Hardware Details M1PWM5 (gen 2)
//  ---Tail Rotor PWM: PF1, J3-10
//
//! \return None.
void
initPWM (void);

#endif /* PWM_H_ */
