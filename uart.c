// *******************************************************
// 
// uart.c
//
// Support for sending data through UART to be read in computer
// terminal.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/22/2023
// *******************************************************

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/debug.h"
#include "driverlib/pin_map.h"
#include "utils/ustdlib.h"
#include "stdio.h"
#include "stdlib.h"
#include "uart.h"

// *******************************************************
// initUART: Initialises the UART to 8 bits, 1 stop bit
// and no parity. 
//
//! \return None.
void
initUART (void)
{
    //
    // Enable GPIO port A which is used for UART0 pins.
    //
    SysCtlPeripheralEnable(UART_USB_PERIPH_UART);
    SysCtlPeripheralEnable(UART_USB_PERIPH_GPIO);
    //
    // Select the alternate (UART) function for these pins.
    //
    GPIOPinTypeUART(UART_USB_GPIO_BASE, UART_USB_GPIO_PINS);
    GPIOPinConfigure (GPIO_PA0_U0RX);
    GPIOPinConfigure (GPIO_PA1_U0TX);

    UARTConfigSetExpClk(UART_USB_BASE, SysCtlClockGet(), BAUD_RATE,
            UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
            UART_CONFIG_PAR_NONE);
    UARTFIFOEnable(UART_USB_BASE);
    UARTEnable(UART_USB_BASE);
}

// *******************************************************
// UARTsend: Transmit the variable through UART
//
//! \param pucBuffer is a pointer to the beginning of a text array.
//
//! \return None.
void
UARTSend (char *pucBuffer)
{

    // Loop while there are more characters to send.
    while(*pucBuffer)
    {
        // Write the next character to the UART Tx FIFO.
        UARTCharPut(UART_USB_BASE, *pucBuffer);
        pucBuffer++;
    }
}

// *******************************************************
// UARTSendData: Larger version of UARTSend for specific use
// in ENCE361 Helicopter project, sending the current altitude, yaw
// and main duty cycle through UART.
//
//! \param altitude is the current altitude as a range from 0 - 1241.
//! \param yaw is a pointer to the array holding the yaw as a percentage.
//! \param main_duty_cycle is the current duty cycle for the main rotor as a percentage.
//
//! \return None.
void
UARTSendData (bool status, int32_t altitude, char* yaw, uint8_t main_duty_cycle, uint8_t tail_duty_cycle)
{
    char text_buffer[17];
    if (status) {
        usprintf(text_buffer, "Flying\r\n");
        UARTSend(text_buffer);
    } else {
        usprintf(text_buffer, "Landing\r\n");
        UARTSend(text_buffer);
    }

    usprintf(text_buffer, "Altitude: %d%%\r\n", altitude);
    UARTSend(text_buffer);

    usprintf(text_buffer, "Yaw: %s\r\n", yaw);
    UARTSend(text_buffer);

    usprintf(text_buffer, "Main Duty Cycle: %d%%\r\n", main_duty_cycle);
    UARTSend(text_buffer);

    usprintf(text_buffer, "Tail Duty Cycle: %d%%\r\n", tail_duty_cycle);
    UARTSend(text_buffer);
    usprintf(text_buffer, "======================\r\n");
    UARTSend(text_buffer);
}

