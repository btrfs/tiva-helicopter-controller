#ifndef ADC_H_
#define ADC_H_

// *******************************************************
// 
// adc.h
//
// Support for the ADC along with functions to calculate
// altitude based on the current mean ADC and the reference
// ground voltage.
// 
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/21/2023
// *******************************************************

//*****************************************************************************
// Constants
//*****************************************************************************
#define BUFFER_SIZE 15
#define ADC_VALUE_RANGE 1241 // 4096/3.3

// *******************************************************
// ADCIntHandler: Runs everytime the ADC interrupt triggers,
// writes to CirBuf the current ADCSequenceData, 
//
//! \note ADCIntClear must be called to clear the sample sequence
//  interrupt source, failure to do so will lead to the interrupt
//  being called immediately upon exit. 
//
//! \return None.
void
initADC (void);

// *******************************************************
// initADC: Initialise the ADC such that is samples at sequence 3.
// also initialise the circbuffer which is to be used when getting
// mean ADC.
//
//! \return None.
void
ADCIntHandler(void);

// *******************************************************
// calcMeanADC: Averages the value in circBuf over a range
//! of \b BUFFER_SIZE 
//
//! \note BUFFER_SIZE is added to the sum to ensure that the
//  final number is rounded.
//
//! \return returns the current Mean ADC as an int. This is
//  range of 0-4096.
int
calcMeanADC(void);

#define getAltitude calcMeanADC

// *******************************************************
// resetZeroBuf: Updates the global zero reference, changing the
//! \b g_zeroRef to the current Mean ADC value, this leads the
//  current altitude output to reset to 0% height
//
//! \return None.
void
resetZeroRef(void);


// *******************************************************
// getAltitudePerc: converts the current mean ADC value to
// a displayable percentage.
//
//! \note negative is used as a drop in voltage means an increase
//  in height. ADC value range is the total range of the ADC 0 - 4096
//  divided by the total range of voltage 0 - 3.3
//
//! \return Percentage of altitude as an int32_t.
int32_t
getAltitudePerc(void);

// *******************************************************
// getAltitudePerc: converts the provided altitude percentage
// into an ADC value.
//
//! \param perc is an altitude percentage.
//
//! \return ADC value of given percentage as an int32_t.
int32_t
convertPercToADC(int32_t);

#endif /* ADC_H_ */
