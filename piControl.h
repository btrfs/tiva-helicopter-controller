#ifndef PICONTROL_H_
#define PICONTROL_H_

// *******************************************************
// 
// piControl.h
//
// Support for the calculation of main and tail motor duty cycle
// using the PI control tuning.
// 
// Authors:
//     A. Sevilla (36541695)
//     D. McNulty (57781020)
// Last modified:	05/22/2023
// *******************************************************

//*****************************************************************************
// Constants
//*****************************************************************************
#define MAIN_CONTROL_GAIN_PROP 600
#define MAIN_CONTROL_GAIN_INT 2000

#define MAIN_OUTPUT_MAX 65
#define MAIN_OUTPUT_MIN 20

#define TAIL_CONTROL_GAIN_PROP 20
#define TAIL_CONTROL_GAIN_INT 225

#define TAIL_OUTPUT_MAX 50
#define TAIL_OUTPUT_MIN 2

// *******************************************************
// calcAltitudeDutyCycle: Calculates the duty cycle required to be outputed
// by the PWM so that the helicopter will reach the desired altitude based 
// on the current altitude.
//
//! \param desired_altitude is the altitude desired through the up and down buttons
//  to reach as a range of 0 - 1241
//
//! \param current_altitude is the current altitude as a range of 0 - 1241.
//
//! \return Duty cycle that is to bet set as an int8_t.
int8_t
calcAltitudeDutyCycle(int16_t, int16_t);

// *******************************************************
// calcYawDutyCycle: Calculates the duty cycle required to be outputed
// by the PWM so that the helicopter will reach the desired yaw based 
// on the current yaw.
//
//! \param desired_yaw is the yaw desired through the left and right buttons
//  to reach as a range of ??????????????
//
//! \param current_yaw is the current yaw as a range of ????????????
//
//! \return Duty cycle that is to bet set as an int8_t.
int8_t
calcYAWDutyCycle(int16_t, int16_t);
#endif /* PICONTROL_H_ */
